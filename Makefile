# Buttons for Linked Pages v0.1.1
# Copyright © 2019, 2020, 2021, 2023, 2024 Boian Berberov
#
# Licensed under the EUPL-1.2 only.
# License text: https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
# SPDX-License-Identifier: EUPL-1.2

OUTPUT_DIR:=extensions

SOURCES=manifest.json background.js

define build-extension =
	@echo "=== $@ ==="
	mkdir -p "$(OUTPUT_DIR)/$@"
	cp -r $@ "$(OUTPUT_DIR)/"
	cp $(SOURCES) "$(OUTPUT_DIR)/$@/"
	sed -i -e "s/__EXTENSION_ID__/$@/" "$(OUTPUT_DIR)/$@/manifest.json"
	echo "$@"'@firefox.clu-os.net' > "$(OUTPUT_DIR)/$@/.web-extension-id"
	web-ext -s "$(OUTPUT_DIR)/$@" -a "$(OUTPUT_DIR)" build
endef

.PHONY: all clean

clean:
	rm -rf $(OUTPUT_DIR)

.PHONY: Top_Page_Button ToC_Button First_Page_Button Prev_Page_Button Up_Button Next_Page_Button Last_Page_Button Index_Button Glossary_Button Search_Page_Button
all:    Top_Page_Button ToC_Button First_Page_Button Prev_Page_Button Up_Button Next_Page_Button Last_Page_Button Index_Button Glossary_Button Search_Page_Button

Top_Page_Button: Top_Page_Button/_locales/*/messages.json Top_Page_Button/_images/*.svg $(SOURCES)
	$(build-extension)

ToC_Button: ToC_Button/_locales/*/messages.json ToC_Button/_images/*.svg $(SOURCES)
	$(build-extension)

First_Page_Button: First_Page_Button/_locales/*/messages.json First_Page_Button/_images/*.svg $(SOURCES)
	$(build-extension)

Prev_Page_Button: Prev_Page_Button/_locales/*/messages.json Prev_Page_Button/_images/*.svg $(SOURCES)
	$(build-extension)

Up_Button: Up_Button/_locales/*/messages.json Up_Button/_images/*.svg $(SOURCES)
	$(build-extension)

Next_Page_Button: Next_Page_Button/_locales/*/messages.json Next_Page_Button/_images/*.svg $(SOURCES)
	$(build-extension)

Last_Page_Button: Last_Page_Button/_locales/*/messages.json Last_Page_Button/_images/*.svg $(SOURCES)
	$(build-extension)

Index_Button: Index_Button/_locales/*/messages.json Index_Button/_images/*.svg $(SOURCES)
	$(build-extension)

Glossary_Button: Glossary_Button/_locales/*/messages.json Glossary_Button/_images/*.svg $(SOURCES)
	$(build-extension)

Search_Page_Button: Search_Page_Button/_locales/*/messages.json Search_Page_Button/_images/*.svg $(SOURCES)
	$(build-extension)
