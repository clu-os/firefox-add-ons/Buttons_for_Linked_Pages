// Buttons for Linked Pages v0.1.1
// Copyright © 2019, 2020, 2021, 2023, 2024 Boian Berberov
//
// Licensed under the EUPL-1.2 only.
// License text: https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
// SPDX-License-Identifier: EUPL-1.2

'use strict';

const HEAD_Examiner_id = 'HEAD_Examiner@firefox.clu-os.net';
const link = new Map();

function onMessageExternalRuntime(message, sender, sendResponse) {
	link.set(message.tabId, message.link.href);
	if (message.link.title)
	{
		browser.browserAction.setTitle(
			{
				'title' : message.link.title + '\n\n' + message.link.href,
				'tabId' : message.tabId
			}
		);
	}
	else
	{
		browser.browserAction.setTitle(
			{
				'title' : message.link.href,
				'tabId' : message.tabId
			}
		);
	}
	browser.browserAction.enable(message.tabId);
}

function onUpdatedTab(tabID, changeInfo, tab) {
	if (changeInfo.status == 'loading')
	{
		browser.browserAction.disable(tabID);
		browser.browserAction.setTitle( { 'title' : null, 'tabId' : tabID } );
	}
}

function onRemovedTab(tabID, removeInfo) {
	link.delete(tabID);
}

function onBrowserAction(tab) {
	browser.tabs.update( tab.id, { 'url' : link.get(tab.id) } );
}

function onExtensionInstalled(info) {
	if (info.id === HEAD_Examiner_id)
	{
		if (info.enabled)
		{
			extensionEnable();
		}
		else
		{
			extensionDisable('noticeHEdisabled');
		}
	}
}
function onExtensionUninstalled(info) {
	if (info.id === HEAD_Examiner_id)
	{
		extensionDisable('noticeHEunavailable');
	}
}
function onExtensionEnabled(info) {
	if (info.id === HEAD_Examiner_id)
	{
		extensionEnable();
	}
}
function onExtensionDisabled(info) {
	if (info.id === HEAD_Examiner_id)
	{
		extensionDisable('noticeHEdisabled');
	}
}

async function extensionEnable() {
	browser.browserAction.setIcon(  {  'path' : null } );
	browser.browserAction.setTitle( { 'title' : null } );
	await browser.runtime.sendMessage( HEAD_Examiner_id, { 'action' : 'scan' } );
}

function extensionDisable(message) {
	browser.browserAction.setIcon( {'path' : '_images/action.disabled.svg'} );
	browser.browserAction.setTitle(
		{ 'title' : browser.i18n.getMessage('defaultTitle') + '\n\n' + browser.i18n.getMessage(message) }
	);
	browser.browserAction.disable();
}

//
// Initialization
//

browser.browserAction.disable();

browser.management.get(HEAD_Examiner_id).then(
	function(info) {
		onExtensionInstalled(info);
	}
	,
	function(reason) {
		extensionDisable('noticeHEunavailable');
	}
);

browser.management.onInstalled.addListener(onExtensionInstalled);
browser.management.onUninstalled.addListener(onExtensionUninstalled);
browser.management.onEnabled.addListener(onExtensionEnabled);
browser.management.onDisabled.addListener(onExtensionDisabled);

browser.tabs.onUpdated.addListener( onUpdatedTab, { 'properties' : ['status'] } );
browser.tabs.onRemoved.addListener(onRemovedTab);

browser.browserAction.onClicked.addListener(onBrowserAction);

browser.runtime.onMessageExternal.addListener(onMessageExternalRuntime);
